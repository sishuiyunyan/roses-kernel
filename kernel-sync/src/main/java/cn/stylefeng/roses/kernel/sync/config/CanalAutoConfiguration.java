package cn.stylefeng.roses.kernel.sync.config;

import cn.stylefeng.roses.kernel.sync.config.properties.CanalProperties;
import cn.stylefeng.roses.kernel.sync.core.listener.CanalStartListener;
import cn.stylefeng.roses.kernel.sync.core.util.CustomSpringContextHolder;
import cn.stylefeng.roses.kernel.sync.modular.cc.SimpleCanalClient;
import cn.stylefeng.roses.kernel.sync.modular.ew.RowDataEntryWrapper;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * canal配置
 *
 * @author fengshuonan
 * @date 2019-01-16-2:19 PM
 */
@Configuration
public class CanalAutoConfiguration {

    /**
     * 项目启动的监听
     *
     * @author fengshuonan
     * @Date 2019/1/16 2:20 PM
     */
    @Bean
    public CanalStartListener applicationReadyListener() {
        return new CanalStartListener();
    }

    /**
     * canal的配置
     *
     * @author fengshuonan
     * @Date 2019/1/16 2:22 PM
     */
    @Bean
    @ConfigurationProperties(prefix = "canal")
    public CanalProperties canalProperties() {
        return new CanalProperties();
    }

    /**
     * canal客户端
     *
     * @author fengshuonan
     * @Date 2019/1/16 2:41 PM
     */
    @Bean
    public SimpleCanalClient canalClient() {
        return new SimpleCanalClient("example");
    }

    /**
     * rowData类型的entry处理者
     *
     * @author fengshuonan
     * @Date 2019/1/17 10:42 AM
     */
    @Bean
    public RowDataEntryWrapper rowDataEntryWrapper() {
        return new RowDataEntryWrapper();
    }

    /**
     * 注入工具
     *
     * @author fengshuonan
     * @Date 2019/12/11 14:51
     */
    @Bean
    public CustomSpringContextHolder customSpringContextHolder() {
        return new CustomSpringContextHolder();
    }

}
